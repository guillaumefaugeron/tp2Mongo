const mongoose = require('mongoose')
const schema   = mongoose.Schema

const UserSchema=new schema({
    name : { type:String, lowercase: true, required:'un nom est obligatoire:)' },
    email: { type:String },
    profilePicture: { type:String },
    pseudo: { type:String },
    favs: [{
        type     : mongoose.Schema.Types.ObjectId,
        ref      : 'mission'
    }],
    pastMissions:[{
        type     : mongoose.Schema.Types.ObjectId,
        ref      : 'mission'
    }]
})

module.exports=Contact=mongoose.model('user',UserSchema)
