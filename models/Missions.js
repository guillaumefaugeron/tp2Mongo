const mongoose = require('mongoose')
const schema   = mongoose.Schema

const MissionSchema=new schema({
    name:{ type:String},
    place : { type:String },
    durationDay: {type:Number},
    skills:[{ type:String }],
    technologies:[{ type:String }],
    isSponsored: { type:Boolean },
    startDate: {type:Date},
    salaryDay:{
        min:{type:Number},
        max:{type:Number}
    },
    description:{ type:String },
    remote: { type:Boolean },
    postingDate:{ type:Date, default: Date.now },
    enterpriseName:{ type:String }
})

module.exports=Mission=mongoose.model('mission',MissionSchema)
