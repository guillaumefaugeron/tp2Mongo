const express=require('express')
const router=express.Router()

const Mission=require('../models/Missions')



// sur GET sans id : il s'agit de la récupération de tous les documents
// localhost:5000/missions/
// [GET] localhost:5000/missions/
router.get("/",(req,res)=>{
    Mission.find({})
    .then(missions=>res.send(missions))
    .catch(err=>console.log(err))
})

router.get("/search",(req,res)=>{
    let {name}=req.query;
    Mission.aggregate([{ $search: { index: 'default',
            text: {
                query: name,
                path: 'name'
            }
        } }])
        .then(missions=>res.send(missions))
        .catch(err=>console.log(err))
})

router.get("/category",(req,res)=>{
    let {tech}=req.query;
    let {skill}=req.query;
    Mission.find({$or:[{skills:skill},{technologies:tech}]})
        .then(missions=>res.send(missions))
        .catch(err=>console.log(err))
})




// sur POST sans id : il s'agit de la création du document
// localhost:5000/missions
// [POST] localhost:5000/missions
router.post("/",(req,res)=>{
    const { name,place,durationDay,skills,technologies,isSponsored,startDate,salaryDay,description,remote,enterpriseName }= req.body
    const newMission=new Mission({
        name,place,durationDay,skills,technologies,isSponsored,startDate,salaryDay,description,remote,enterpriseName
    })
    newMission.save()
    .then(missions=>res.send(missions))
    .catch(err=>console.log(err))
})


// sur GET avec id : il s'agit de la récupération d'un et d'un seul document (s'il existe)
// localhost:5000/missions/6055c2a61bcfb139a404b3a0
// [GET] localhost:5000/missions/6055c2a61bcfb139a404b3a0
router.get("/:_id",(req,res)=>{
    const {_id}=req.params
    Mission.findOne({_id:_id})
      .then(mission=>res.send(mission))
    .catch(err=>console.log(err))
})


// sur PUT avec id : l'édition d'un document
// localhost:5000/missions/6055c2a61bcfb139a404b3a0
router.put("/:_id",(req,res)=>{
    const {_id}=req.params

    const {name,place,durationDay,skills,technologies,isSponsored,startDate,salaryDay,description,remote,enterpriseName}=req.body
    Mission.findOneAndUpdate({_id},{name,place,durationDay,skills,technologies,isSponsored,startDate,salaryDay,description,remote,enterpriseName})
    .then(mission=>res.send(mission))
    .catch(err=>console.log(err))
})


// sur DELETE avec id : la suppression d'un document
//localhost:5000/missions/6055c2a61bcfb139a404b3a0
router.delete("/:_id",(req,res)=>{
    const {_id}=req.params
    Mission.findOneAndDelete({_id})
    .then(missions=>res.send("success"))
    .catch(err=>console.log(err))
})


module.exports=router
