const express=require('express')
const router=express.Router()

const User=require('../models/Users')



// sur GET sans id : il s'agit de la récupération de tous les documents
// localhost:5000/users/
// [GET] localhost:5000/users/
router.get("/",(req,res)=>{
    let {embed}=req.query;
    if(embed == "true"){
        User.find().populate("favs").populate("pastMissions")
        .then(users=>res.send(users))
        .catch(err=>console.log(err))
    }else{
        User.find()
            .then(users=>res.send(users))
            .catch(err=>console.log(err))
    }

})

// sur POST sans id : il s'agit de la création du document
// localhost:5000/users
// [POST] localhost:5000/users
router.post("/",(req,res)=>{
    const { name,email,profilePicture,pseudo,favs,pastMissions }= req.body
    const newUser=new User({
        name,email,profilePicture,pseudo,favs,pastMissions
    })
    newUser.save() // du mongoose
    .then(users=>res.send(users))
    .catch(err=>console.log(err))
})


// sur GET avec id : il s'agit de la récupération d'un et d'un seul document (s'il existe)
// localhost:5000/users/6055c2a61bcfb139a404b3a0
// [GET] localhost:5000/users/6055c2a61bcfb139a404b3a0
router.get("/:_id",(req,res)=>{
    const {_id}=req.params
    User.findOne({_id:_id})
      .then(user=>res.send(user))
    .catch(err=>console.log(err))
})


// sur PUT avec id : l'édition d'un document
// localhost:5000/users/6055c2a61bcfb139a404b3a0
router.put("/:_id",(req,res)=>{
    const {_id}=req.params

    const {name,email,profilePicture,pseudo,favs,pastMissions}=req.body
    User.findOneAndUpdate({_id},{name,email,profilePicture,pseudo,favs,pastMissions})
    .then(user=>res.send(user))
    .catch(err=>console.log(err))
})


// sur DELETE avec id : la suppression d'un document
//localhost:5000/users/6055c2a61bcfb139a404b3a0
router.delete("/:_id",(req,res)=>{
    const {_id}=req.params
    User.findOneAndDelete({_id})
    .then(users=>res.send("success"))
    .catch(err=>console.log(err))
})


module.exports=router
